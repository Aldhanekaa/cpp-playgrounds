#include "factorialize.h"

int factorialize(int factor)
{
    if (factor == 0)
    {
        return 1;
    }
    else
    {
        return factor * factorialize(factor - 1);
    }
}
