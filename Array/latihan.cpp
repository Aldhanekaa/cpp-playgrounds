#include <iostream>
#include <array>
using namespace std;

int main() {
	array<int, 10> nilaiSiswa;

	cout << "tolong masukan nilai-nilai siswa" << endl;

	cout << "\n\n=====================================" << endl;
	cout << "== Nilai = Input ====================" << endl;

	for (int i = 0; i <= nilaiSiswa.size(); i++) {

		if ( i != 10) {

		 cout << " " << i * 10 << " - " << (i*10) + 9;

		 if (i != 0) {
		 	cout << " : ";
		 }else {
		 	cout << "   : ";
		 }
			
		}else{
			cout << " " << i * 10 << "     : ";
		}

		cin >> nilaiSiswa[i];

	}	

	// cout << nilaiSiswa[10];

	cout << "\n\n========== Grafik ==========\n";
	for (int i = 0; i <= nilaiSiswa.size(); i++) {
		cout << "\n";
		if ( i != 10) {

		 cout << " " << i * 10 << " - " << (i*10) + 9;

		 if (i != 0) {
		 	cout << " : ";
		 }else {
		 	cout << "   : ";
		 }
			
		}else{
			cout << " " << i * 10 << "     : ";
		}

		for (int j = 1; j <= nilaiSiswa[i]; j++) {
			cout << "*";
		}
	}
}