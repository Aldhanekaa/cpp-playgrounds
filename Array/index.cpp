#include <iostream>
#include <array>
using namespace std;

int main()
{
    int iniArraySaya[] = {1,2,3,4,5};
    cout << iniArraySaya; // memory address

    int *ptr = iniArraySaya; // memory address
    *(ptr + 2) = 5; // mengganti array index 2 menjadi 5. ptr (memory address) + 2 int (4 byte) = memory address + 4 byte

    return 0;
}