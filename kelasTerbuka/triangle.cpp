#include <iostream>
using namespace std;

int main() {
	int n = 0;
	char pola;

	system("clear");

	do {
		if (n >= 10) {
			system("clear");
			cout << "Please give an input less than 10: ";
		}else cout << "Masukan panjang pola: ";
		cin >> n;

		if (n < 10) {
			cout << "Masukan gambar pola: ";
			cin >> pola;
		}
	}while (n >= 10);


	cout << "========== pola 1 ==========\n";

	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= i; j++) {
			cout << pola << " ";
		}
		cout << endl;
	}

	cout << "========== pola 2 ==========\n";
	for (int i = n; i >= 1; i--) {
		for (int j = i; j >= 1; j--) {
			cout << pola << " ";
		}

		cout << endl;
	}
	
	cout << "========== pola 3 ==========\n";
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < i; j++) {
			cout << " ";
		}
		for (int j = n; j > i; j--) {
			cout << pola;
		} 
		cout << endl;
	}

	// cout << "========== pola 4 ==========\n";
	for (int i = 1; i <= n; i++) {
		for (int j = n; j > i; j--) {
			cout << " ";
		}
		for (int j = 1; j <= i; j++) {
			cout << pola;
		} 
		cout << endl;
	}

	cout << "========== pola 5 ==========\n";
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j < i; j++) {
			cout << " ";
		}
		for (int j = n; j >= i; j--) {
			cout << pola << " ";
		} 
		cout << endl;
	}

	// cout << "========== pola 6 ==========\n";
	for (int i = 1; i <= n; i++) {
		for (int j = n; j > i; j--) {
			cout << " ";
		}
		for (int j = 1; j <= i; j++) {
			cout << pola << " ";
		} 
		cout << endl;
	}


	cout << "========== pola 7 ==========\n";
	for (int i = 0; i < n; i++) {
		for (int j = n; j > i; j--) {
			cout << " ";
		}
		for (int j = 0; j <= i; j++) {
			cout << n-i << " ";


		} 
		cout << endl;
	}

	cout << "========== pola 8 ==========\n";
	for (int i = 0; i < n; i++) {
		for (int j = n; j > i; j--) {
			cout << " ";
		}
		int N = n - i;
		int isNequalTon = false;
		for (int j = 0; j <= i; j++) {
			cout << N << " ";

			if (N == n && j != 0) 
				isNequalTon = true;
			

			if (isNequalTon) 
				N--;
			else 
				N++;


		} 
		cout << endl;
	}


	cin.get();
}

// g++ triangle.cpp -o triangle && ./triangle