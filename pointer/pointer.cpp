#include <iostream>
#include <cmath>
using namespace std;

/*
 * @param pointer
 */

void cubic(int *b) {
	static int a = 0;
	*b = pow(*b, 3);
	cout << ++a << endl;
}

int main() {
	int a = 2;

	cout << "A: " <<  a << endl;

	cubic(&a);

	cout << "A: "<<  a << endl;

	cubic(&a);

	cubic(&a);

	cubic(&a);


	// cubic('a');
}

// g++ pointer.cpp -o pointer