#include <iostream>
using namespace std;

int main() {
	int a = 5;

	// pointer 
	int *b = &a;

	cout << "memory address of a: " << &a << endl;
	cout << "memory address of b: " << b << endl;
	cout << "=== VALUE ====\n";
	cout << "a: " << a << endl;
	cout << "b: " << *b << endl;

	*b = 10;

	cout << "VALUE CHANGED \n";

	// test
	cout << "memory address of a: " << &a << endl;
	cout << "memory address of b: " << b << endl;
	cout << "=== VALUE ====\n";
	cout << "a: " << a << endl;
	cout << "b: " << *b << endl;

	cout << &b;

	// refrence
	cout << "\n======== refrence ========\n";
	int c = 5;
	int &d = c;

	cout << "memory address of c: " << &c << endl;
	cout << "memory address of d: " << &d << endl;
	cout << "=== VALUE ====\n";
	cout << "c: " << c << endl;
	cout << "d: " << d << endl;

	d = 10;

	cout << "VALUE CHANGED \n";

	// test
	cout << "memory address of c: " << &c << endl;
	cout << "memory address of d: " << &d << endl;
	cout << "=== VALUE ====\n";
	cout << "a: " << c << endl;
	cout << "d: " << d << endl;
	cout << &d;

}

// g++ indx.cpp -o indx && ./indx