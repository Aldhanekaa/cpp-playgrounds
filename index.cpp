 #include <iostream>
using namespace std;

void Swap(int& a, int& b) {
    int temp = a;
    a = b;
    b = temp;
}

int main() {
    int a = 5, b=7;

    cout << a << " - " << b << endl;
    Swap(a, b);
    cout << a << " - " << b << endl;
}

// g++ index.cpp -o indexCpp && ./indexCpp