#include <iostream>

using namespace std;

// g++ bmi.cpp -o bmi && ./bmi

int main()
{
    // bmi = weight / height ** 2

    float weight, height, bmi;
    char confirm = 'N';
    char b = 'N';

    while (confirm == 'N' || confirm == 'n' || b == 'Y')
    {
        if (weight && height)
            system("clear");

        cout << "may I know your height please? ";
        cin >> height;

        cout << "may I know your weight please? ";
        cin >> weight;

        system("clear");

        cout << "height: " << height << endl;
        cout << "weight: " << weight << endl;

        cout << "\nAre those your informations? [Y/N] \n";
        cout << "==================\n";

        cin >> confirm;
    }

    system("clear");

    bmi = weight / (height * height);
    cout << "Your BMI is: " << bmi << endl;

    cout << "wanna try again? [Y/N] \n";
    cin >> b;
    if (b == 'Y')
    {
        main();
    }
}
