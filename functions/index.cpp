// index.cpp
#include <iostream>
using namespace std;

int c = 10;

void incrementC();
void autoKeyword();

int main() {
	int c = 20;

	cout << c << endl;

	cout << ::c << endl;

	cout << "incrementC()" << endl;
	incrementC();
	cout << "let see what value in global variable c:" << ::c << endl;
	autoKeyword();
}

void incrementC() {
	::c++;

	cout << ::c << endl;
}

void autoKeyword()
{
	register int a = 10;

	printf("a is %d", a);
}

// g++ index.cpp -o index && ./index