//Working of Implicit type-conversion

#include <iostream>
using namespace std;

int main() {

   int num_int;
   double num_double = 9.99;

   // implicit conversion
   // assigning a double value to an int variable
   num_int = num_double;

   cout << "num_int = " << num_int << endl;
   cout << "num_double = " << num_double << endl;

   cout << "============= TYPES ============= \n";

   cout << "num_int = " << typeid(num_int).name() << endl;
   cout << "num_double = " << typeid(num_double).name() << endl;

   /*
	According to Programmiz : Here, the double value is automatically converted to int by the compiler before it is assigned to the num_int variable. This is also an example of implicit type conversion.
   */

   return 0;
}