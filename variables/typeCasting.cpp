#include <iostream>
using namespace std;

int main() {
	char vowel;

	cout << "Please give an input for a vowel" << "\n>";
	cin >> vowel;

	switch(vowel) {
		case 'o':
		case 'u':
		case 'e':
		case 'i':
		case 'a':
			cout << "Right!";
			break;
		default:
			cout << "INCORRECT!";
			break;
	}

	// initializing int variable
int num_int = 65;

// declaring double variable
double num_double;

char A;

num_int *= 2.2;

// converting from int to double
num_double = (double)num_int;

A = char(num_int);

cout << typeid(num_int).name() << "\nA: " << A;

	return 0;
}