#include <iostream>
using namespace std;

int main() {
	unsigned int a;

	cout << "Give an input for unsigned int a variable: ";
	cin >> a;
	cout << "a's value is " << a << endl;

	cout << "Implicit Conversion / Automatic conversion \n";
	int A = 10;
	double A_Double, Temp;

	A_Double = A;

	cout << "Now A is: " << A << endl; 

	cout << "Type of variable A: " << typeid(A).name() << "\ntype of variable A_Double: " << typeid(A_Double).name() << "\ntypeof Temp variable: " << typeid(Temp).name();

}