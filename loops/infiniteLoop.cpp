#include <iostream>
using namespace std;
// infiniteLoop.cpp


int main() {
	int counter;
	cout << "please give an input for the counter variable: ";
	cin >> counter;

	while(counter <= 500)
	{
		if (counter % 2 == 0)
			cout << "Counter is an enven : " << counter << endl;
		else 
			cout << "Counter is an odd : " << counter << endl;

		counter++;
	}
}

// infiniteLoop.cpp | g++ infiniteLoop.cpp -o infiniteLoop && ./infiniteLoop