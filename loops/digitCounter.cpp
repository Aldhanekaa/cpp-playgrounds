#include <iostream>
using namespace std;

//digitCounter.cpp

int main() {
	int number = 0, counter = 0;

	cout << "Number: ";

	while(number == 0) {
		cin >> number;
		if (number == 0) cout << "Please give it greater than 0 : ";
	}

	while (number > 0)
	{	
		number /= 10;
		counter++;
	}

	cout << "it's " << counter << " digits";

}

// g++ digitCounter.cpp -o digitCounter && ./digitCounter