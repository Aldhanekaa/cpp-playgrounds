// reverseNumber.cpp

#include <iostream>
using namespace std;

int main() {
		int number, reversedNumber = 0;

	cout << "Number: ";

	do {
		cin >> number;
		if (number == 0) cout << "Please give it greater than 0 : ";
	}while(number == 0);

	system("clear");

	
	cout << "number: " << number << endl;
	reversedNumber *= 10;
	cout << "reversedNumber: " << reversedNumber << endl;
	reversedNumber += number % 10;
	cout << "reversedNumber: "<< reversedNumber << endl;
	number /= 10;
	

	cout << "Number: " << number << "\nReversed Number: "<< reversedNumber;

}

// g++ reverseNumber.cpp -o reverseNumber && ./reverseNumber