// pin.cpp

#include <iostream>
using namespace std;

int main() {
	int userPIN = 1234, pin = 1234, errorCounter = 0;

	do {
		cout << "Enter your PIN: ";
		cin >> pin;

		if (userPIN != pin )  {
			system("clear");
			cout << "Invalid PIN\n";
			cout << "USER" << userPIN << "\nPIN" << pin<<endl;
			errorCounter++;
		}

	}while(errorCounter <3 && pin!=userPIN);

	if (errorCounter < 3)
		cout << "Please wait..";
	else
		cout << "You have been blocked";
}

// g++ pin.cpp -o pin && ./pin